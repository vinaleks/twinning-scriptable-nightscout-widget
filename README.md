Totally customizable nightscout widget (with a few surprises tucked in)!

The color of the widget is custom (info below on how to change it).. until you go too high or too low then it starts to fade from dark red to bright red the more out of range you are.  Go below 70 and you'll get a juicebox icon on your widget, go higher than 150 and your emoji face starts to get more scared. (One of the emojis used is ios15+ so if it doesn't work, update your phone)

If you happen to twin with your diabestie? Well, all of a sudden your widget has changed colors! (the teal it defaults to was my son's favorite color at the time) AND you get a bonus emoji that pops up! 

There's even some fun emojis hiding based on readings.  (90 gets you a target for example - change this one as needed if you'd like).  There was a fair bit of snark involved when you hit 200 and 300, at that point it's either laugh or cry and I would rather laugh.. then rage bolus. 

Everything from the colors, the fonts, the font sizes is totally custom (even the emoji when a twinning occurs). 

Because I can't control things, this is scriptable based and therefore only iphones (sorry Android) and does require scriptable to make it work.  While you're downloading that app, look at all of the other cool things it can do as well, it's a fun one to have. 



Here's how to get this up and running on your phone: 

Download https://scriptable.app

Create a new script by tapping the + in the upper right of your screen. 

Copy twinningwidget.js into the new script

Customize as indicated below, if desired. 

Line 2: enter primary Nightscout’s link in place of the underscores

Line 4: enter diabestie’s nightscout link in place of the underscores

Line 34: enter name of primary T1D (Adjust to non upper case if desired)

Line 35: change font or font size (“Brookie” is in “SavoyerLetPlain” font, whatever font you choose needs to be an ios font.  List is here: www.iosfonts.com)

Line 48, enter hex code of color of choice, default is white (or black if your phone is on dark mode) This will be the color of the widget when BG is in range.  Hexidecimal colors online can be found here: www.encycolorpedia.com.  The blue was the sky just before sunset in NYC March of 2020 (I'm not extra at all, I needed it to match my phone aesthetic, ok?)

Line 116: Change emoji if desired to indicate twinning readings (default is a blonde female presenting person holding hands with a brunette male presenting person.. change this to whatever you prefer). 

Line 117: change hex code to color of choice to indicate twinning

Lines 127-130: IOB and COB if desired, simply delete the // and it will reflect what is logged into your nightscout.  There may not be room for iob/cob and the emoji, we aren't looping (yet) so I can't totally test it.  If there's not room, you can be super extra and run one with iob/cob and one with emojis, totally up to you. 

Add widget to your IOS device.  I've only tested this as a small widget, I'm sure if the dishes pile up enough, I'll figure out a medium widget as a avoidance to the dishes.. so I'll say "coming soon" on that one. 